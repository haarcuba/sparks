import socket
import sys

def send( server, filename ):
	sock = socket.socket()
	sock.connect( server )
	f = open( filename, 'rb' )
	data = 'non-empty'
	while len( data ) > 0:
		data = f.read( 4096 )
		sock.send( data )
		

if __name__ == '__main__':
	ip = sys.argv[ 1 ]
	filename = sys.argv[ 3 ]
	send( ( ip, 3333 ), filename )
