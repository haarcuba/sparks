import socket
import sys

class Receiver( object ):
	def __init__( self, filename ):
		self._filename = filename

	def go( self ):
		listener = socket.socket()
		listener.setsockopt( socket.SOL_SOCKET, socket.SO_REUSEADDR, 1 )
		listener.bind( ( '', 3333 ) )
		listener.listen( 3 )

		connection, peer = listener.accept()
		print 'got connection from ', peer
		self._saveToFile( connection )

	def _saveToFile( self, connection ):
		totalBytes = 0
		with open( self._filename, 'wb' ) as f:
			data = 'non-empty-string'
			while len( data ) > 0:
				data = connection.recv( 4096 )
				f.write( data )
				totalBytes += len( data )

		print 'saved %d bytes' % totalBytes

if __name__ == '__main__':
	receiver = Receiver( sys.argv[ 1 ] )
	receiver.go()
