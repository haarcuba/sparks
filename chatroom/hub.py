import threading
import select
import Queue
import logging
import socket

class Hub( object ):
	def __init__( self ):
		self._queue = Queue.Queue()
		self._clients = {}
		self._bySocket = {}

	def add( self, socket, ip ):
		nameString = socket.recv( 4096 )
		unused, name = nameString.split()
		self._queue.put( ( socket, name ) )

	def go( self ):
		while True:
			self._addNewClients()
			self._dispatchMessages()

	def _addNewClients( self ):
		while self._queue.qsize() > 0:
			socket, name = self._queue.get()
			logging.info( 'client %s added' % name )
			self._clients[ name ] = socket
			self._bySocket[ socket ] = name
			socket.send( 'currently connected: %s' % self._clients.keys() )
			self._broadcast( 'server', '%s just connected' % name )

	def _dispatchMessages( self ):
		sockets = self._clients.values()
		read, write, exception = select.select( sockets, [], sockets, 1 )
		for socket in read:
			data = socket.recv( 4096 )
			if data == '':
				socket.close()
				name = self._bySocket[ socket ]
				del self._bySocket[ socket ]
				del self._clients[ name ]
				self._broadcast( 'server', '%s has disconnected' % name )
				continue
			words = data.split()
			destination, message = words[ 0 ], ' '.join( words[ 1: ] )
			source = self._bySocket[ socket ]
			if destination == 'all':
				self._broadcast( source, message )
				continue
			peer = self._clients[ destination ]
			logging.info( 'from %s to %s: %s' % ( source, destination, message ) )
			peer.send( '%s: %s' % ( source, message ) )

	def _broadcast( self, source, message ):
		logging.info( 'from %s to all: %s' % ( source, message ) )
		for name, socket in self._clients.items():
			if name == source:
				continue
			socket.send( '%s->all: %s' % ( source, message ) )

class Server( object ):
	def __init__( self, hub ):
		self._hub = hub

	def go( self ):
		sock = socket.socket()
		sock.setsockopt( socket.SOL_SOCKET, socket.SO_REUSEADDR, 1 )
		sock.bind( ('', 2222 ) )
		sock.listen( 5 )
		logging.info( 'listening' )

		while True:
			connection, peer = sock.accept()
			self._hub.add( connection, peer[ 0 ] )

if __name__ == '__main__':
	logging.basicConfig( level = logging.DEBUG )
	hub = Hub()
	server = Server( hub )
	serverThread = threading.Thread( target = server.go )
	serverThread.daemon = True
	serverThread.start()
	hub.go()
