import socket
import logging
import thread
import sys
import threading
import sys
import argparse

class Client( object ):
	def __init__( self, server, name ):
		self._socket = socket.socket()
		self._socket.connect( server )
		self._name = name
		logging.info( 'connected to %s' % str( server ) )
		self._handshake()

	def _write( self, string ):
		sys.stdout.write( string )
		sys.stdout.flush()

	def _handshake( self ):
		self._socket.send( 'name: %s' % self._name )
		answer = self._socket.recv( 4096 )
		self._write( 'server said: %s\n' % answer )

	def _reader( self ):
		while True:
			data = self._socket.recv( 4096 )
			if data == '':
				self._socket.close()
				quit()
			self._write( '\n%s\n' % data )
			self._write( '%s> ' % self._name )

	def go( self ):
		reader = threading.Thread( target = self._reader )
		reader.daemon = True
		reader.start()
		
		while True:
			self._write( '%s> ' % self._name )
			input = raw_input()
			self._socket.send( input )

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument( 'ip' )
	parser.add_argument( 'port', type = int )
	parser.add_argument( 'name' )
	arguments = parser.parse_args()
	server = arguments.ip, arguments.port
	client = Client( server, arguments.name )
	try:
		client.go() 
	except:
		pass
