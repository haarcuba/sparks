import socket
import sys
import argparse

def go( myPort, peer ):
	sock = socket.socket( socket.AF_INET, socket.SOCK_DGRAM )
	sock.bind( ( '', myPort ) )
	sock.connect( peer )
	while True:
		input = raw_input()
		sock.send( input )
		answer = sock.recv( 4096 )
		print "got: ", answer

	
if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument( 'peerIP' )
	parser.add_argument( 'peerPort', type = int )
	parser.add_argument( 'myPort', type = int )
	arguments = parser.parse_args()
	go( arguments.myPort, ( arguments.peerIP, arguments.peerPort ) )
