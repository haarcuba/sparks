import socket
import sys
import argparse
import threading

def receiver( myPort ):
	sock = socket.socket( socket.AF_INET, socket.SOCK_DGRAM )
	sock.bind( ( '', myPort ) )
	while True:
		message = sock.recv( 4096 )
		print "got: ", message
		
def sender( peer ):
	sock = socket.socket( socket.AF_INET, socket.SOCK_DGRAM )
	sock.connect( peer )
	while True:
		input = raw_input()
		sock.send( input )
	
if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument( 'peerIP' )
	parser.add_argument( 'peerPort', type = int )
	parser.add_argument( 'myPort', type = int )
	arguments = parser.parse_args()

	receivingThread = threading.Thread( target = receiver, args = [ arguments.myPort ] )
	receivingThread.daemon = True
	receivingThread.start()

	sender( ( arguments.peerIP, arguments.peerPort ) )
