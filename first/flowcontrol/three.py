# vim: set fileencoding=utf8 :
while True:
	number = input( 'please enter an even number: ' )
	if number % 2 == 0:
		print "thanks, that's just what I wanted"
	else:
		print "no! bad! I said an *even* number, does %s look even to you???" % number
		print "now, you will suffer the consequences %s times!" % number
		for counter in range( number ):
			print 'bad! (no. %s)' % ( counter + 1 )

		print "OK, now don't do it again."
