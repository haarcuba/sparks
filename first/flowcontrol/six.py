# vim: set fileencoding=utf8 :

def ok( number ):
	return number % 2 == 0 and number % 3 == 0

def badboy( number ):
	print "no! bad! I said an *even* number, does %s look even to you???" % number
	print "now, you will suffer the consequences %s times!" % number
	for counter in range( number ):
		print 'bad! (no. %s)' % ( counter + 1 )

	print "OK, now don't do it again."

def main():
	number = None
	while number != 0:
		number = input( 'please enter an even number (0 to quit): ' )
		if ok( number ):
			print "thanks, that's just what I wanted"
		else:
			badBoy( number )

main()
